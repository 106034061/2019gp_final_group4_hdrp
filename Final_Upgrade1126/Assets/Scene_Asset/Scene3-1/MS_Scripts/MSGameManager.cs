﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class MSGameManager : MonoBehaviour {
  public MSBoard mBoard;
  public static MSCell[,] mAllCells = new MSCell[50, 50];
  public static int sideLen = 21;
  public static int sidePixels = 40;
  public static int[] ans = new int[2500];
  public static int[] cur = new int[2500];
  public static int[] dx = { 0, 1, 0, -1, 1, 1, -1, -1 };
  public static int[] dy = { 1, 0, -1, 0, 1, -1, 1, -1 };

  void Start() { mBoard.Create(mAllCells, ans); }

  public static void rightClickCell(GameObject clickedCell) {
    MSGameManager mng = GameObject.Find("MS_GameManager").gameObject.GetComponent<MSGameManager>();
    int px = clickedCell.GetComponent<MSCell>().mBoardPosition.x;
    int py = clickedCell.GetComponent<MSCell>().mBoardPosition.y;
    if (px >= sideLen || py >= sideLen) return;
    if (cur[px * sideLen + py] == 2) return; // 2 is clicked
    cur[px * sideLen + py] ^= 1;
    string gol = cur[px * sideLen + py] == 1 ? "flag" : "Outline0";
    for (int i = 0; i < clickedCell.transform.childCount; ++i) {
      GameObject t = clickedCell.transform.GetChild(i).gameObject;
      t.SetActive(t.name == gol ? true : false);
    }
  }

  public static void leftClickCell(GameObject clickedCell) {
    MSGameManager mng = GameObject.Find("MS_GameManager").gameObject.GetComponent<MSGameManager>();
    int px = clickedCell.GetComponent<MSCell>().mBoardPosition.x;
    int py = clickedCell.GetComponent<MSCell>().mBoardPosition.y;
    if (px >= sideLen || py >= sideLen) return;
    if (cur[px * sideLen + py] >= 1) return; // 2 is clicked, 1 is flagged
    Queue<int> que = new Queue<int>();
    que.Enqueue(px * sideLen + py);
    cur[px * sideLen + py] = 2;
    while (0 < que.Count) {
      int x = que.Peek() / sideLen;
      int y = que.Peek() % sideLen;
      que.Dequeue();
      int truth = ans[x * sideLen + y];
      string struth = truth == -1 ? "bomb" : "num" + truth.ToString();
      for (int i = 0; i < mAllCells[x, y].transform.childCount; ++i) {
        GameObject t = mAllCells[x, y].transform.GetChild(i).gameObject;
        t.SetActive(t.name == struth ? true : false);
      }
      if (ans[x * sideLen + y] == 0) {
        for (int d = 0; d < 4; ++d) {
          int nx = x + dx[d];
          int ny = y + dy[d];
          if (nx < 0 || ny < 0 || nx == sideLen || ny == sideLen) continue;
          if (cur[nx * sideLen + ny] != 2) {
            que.Enqueue(nx * sideLen + ny);
            cur[nx * sideLen + ny] = 2;
          }
        }
      }
    }
  }
}

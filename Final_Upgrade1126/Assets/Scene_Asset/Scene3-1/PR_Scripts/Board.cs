using UnityEngine;
using UnityEngine.UI;

public class Board : MonoBehaviour {
  public GameObject mCellPrefab;
  public Cell[,] mAllCells = new Cell[50, 50];

  public void Create() {
    for (int y = 0; y < GameManager.sideLen + 1; ++y) {
      for (int x = 0; x < GameManager.sideLen + 1; ++x) {
        GameObject newCell = Instantiate(mCellPrefab, transform);
        RectTransform rectTransform = newCell.GetComponent<RectTransform>();
        int sp = GameManager.sidePixels;
        rectTransform.anchoredPosition = new Vector2(x * sp + sp / 2, y * sp + sp / 2);
        mAllCells[x, y] = newCell.GetComponent<Cell>();
        mAllCells[x, y].Setup(new Vector2Int(x, y), this);
      }
    }

    if (GameManager.sideLen == 21) {
      int[] magic = {2086370, 1071260, 1528260, 1529908, 1528307, 1066277, 2087214, 7838, 477417, 1933470, 313636, 561303, 2062506, 1792, 2086271, 1072449, 1525085, 1528413, 1525853, 1069633, 2083199};
      for (int r = 0; r < GameManager.sideLen; ++r) {
        int s = 0;
        for (int c = 0; c < GameManager.sideLen; ++c) s += (magic[r] >> c & 1);
        foreach (Transform child in mAllCells[r, GameManager.sideLen].transform) {
          child.gameObject.SetActive(child.gameObject.name == "num" + s.ToString());
        }
      }
      for (int r = 0; r < GameManager.sideLen; ++r) {
        int s = 0;
        for (int c = 0; c < GameManager.sideLen; ++c) s += (magic[c] >> r & 1);
        foreach (Transform child in mAllCells[GameManager.sideLen, r].transform) {
          child.gameObject.SetActive(child.gameObject.name == "num" + s.ToString());
        }
      }
      for (int r = 0; r < GameManager.sideLen; ++r) {
        for (int c = 0; c < GameManager.sideLen; ++c) {
          if ((magic[r] >> c & 1) == 1) {
            for (int i = 0; i < 2; ++i) {
              GameObject t = mAllCells[r, c].transform.GetChild(i).gameObject;
              t.SetActive(t.activeSelf ? false : true);
            }
          }
        }
      }
    }
  }
}

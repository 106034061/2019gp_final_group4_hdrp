﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class loadQRCode : MonoBehaviour {
  public string code = ""; 
  public string baseurl = "http://m1n1server.herokuapp.com";
  public float timer = 0f;
  public float requestInterval = 3f;
  public string result = "";

  void Start() {
    const string glyphs= "abcdefghijklmnopqrstuvwxyz0123456789";
    for (int i = 0; i < 4; ++i) code += glyphs[Random.Range(0, glyphs.Length)];
    StartCoroutine(DownloadImage(baseurl + "/generate?code=" + code));
  }

  void FixedUpdate() {
    timer += Time.deltaTime;
    if (timer > requestInterval) {
      StartCoroutine(GetText(baseurl + "/query?code=" + code));
      timer = 0f;
    }
    if (result == "true") SceneManager.LoadScene("Scene1-1");
  }

  IEnumerator DownloadImage(string MediaUrl) {
    UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
    yield return request.SendWebRequest();
    if(request.isNetworkError || request.isHttpError) {
      Debug.Log(request.error);
    } else {
      gameObject.GetComponent<RawImage>().texture = ((DownloadHandlerTexture) request.downloadHandler).texture;
    }
  }

  IEnumerator GetText(string url) {
    using (UnityWebRequest www = UnityWebRequest.Get(url)) {
      yield return www.Send();
      if (www.isNetworkError || www.isHttpError) {
        Debug.Log(www.error);
      } else {
        result = www.downloadHandler.text;
      }
    }
  }
}

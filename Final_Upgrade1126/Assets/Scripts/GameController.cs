﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public string Level = "Scene1-1";
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log("Next Level");
            if( Level == "Scene1-1"){
                SceneManager.LoadScene("scene1-2(1)");
            }
            else if(Level == "Scene1-2_B"){
                SceneManager.LoadScene("scene1-3B");
            }
            else if(Level == "Scene1-2_W"){
                SceneManager.LoadScene("scene1-3W");
            }
            else if(Level == "Scene1-3_B"){
                SceneManager.LoadScene("scene2-2");
            }
            else if(Level == "Scene1-3_W"){
                SceneManager.LoadScene("scene2-2");
            }
            else if(Level == "Scene2-2"){
                SceneManager.LoadScene("scene3-1");
            }
            else if(Level == "Scene3-1"){

            }
        }
    }
}

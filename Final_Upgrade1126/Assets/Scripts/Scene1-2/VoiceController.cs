﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceController : MonoBehaviour
{ 
    public int Voice_Mode = 1;
    public AudioClip Voice_Clip;
    public AudioSource Voice;
    public GameObject Pivot;
    public GameObject Player;
    private float Current_Distance;
    private float Previous_Distance;
    private float Delta_Distance;

    static private float Voice_Volume = (float)0.05;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("VoiceUpdate", 0.5f, 3.0f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // VoiceUpdate();
    }
    void VoiceUpdate(){
        Current_Distance = Vector3.Distance(Player.transform.position, Pivot.transform.position);
        Delta_Distance = Current_Distance - Previous_Distance;
        if(Voice_Mode == 1){
            if(Delta_Distance > 0){
                Voice.GetComponent<AudioSource>().volume -= Voice_Volume;
            }
            else if( Delta_Distance < 0){
                Voice.GetComponent<AudioSource>().volume += Voice_Volume;
            }
        }
        else if(Voice_Mode == 0){
            if(Delta_Distance > 0){
                Voice.GetComponent<AudioSource>().volume += Voice_Volume;
            }
            else if( Delta_Distance < 0){
                Voice.GetComponent<AudioSource>().volume -= Voice_Volume;
            }
        }
        Previous_Distance = Current_Distance;
    }
}

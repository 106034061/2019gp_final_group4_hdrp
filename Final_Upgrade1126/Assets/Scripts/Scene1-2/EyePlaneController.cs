﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EyePlaneController : MonoBehaviour
{
    public GameObject Player;
    public AudioClip Narration;
    public AudioSource Narration_Voice;
    private float Current_Distance;
    public GameObject Laser_Light;
    public float Eye_Distance = 4;
    public float Invoke_time = 5.0f;
    private int Random_Number;
    void Start()
    {
        Player = GameObject.Find("PlayerController/first_person_controller/Movingggg");
        Narration_Voice = this.gameObject.GetComponent<AudioSource>();
        Laser_Light = gameObject.transform.GetChild(0).gameObject;
        // Laser_Light.SetActive(false);
        InvokeRepeating("LightUpdate", 0.0f, 2.0f);

    }
    void Awake(){
        // Laser_Light.SetActive(false);

    }
    // Update is called once per frame
    void Update()
    {
        NearUpdate();

    }
    void LightUpdate(){
        Random_Number = Random.Range(0, 100);
        if(Random_Number > 50){
            if(Laser_Light.activeSelf ){
                Laser_Light.SetActive(false);
            }
            else {
                Laser_Light.SetActive(true);
            }
        }

    }
    void NearUpdate(){
        Current_Distance = Vector3.Distance(Player.transform.position, transform.position);
        // if(Current_Distance < 1.0f){
        //     Narration_Voice.PlayOneShot(Narration);
        //     Laser_Light.SetActive(true);
        // }
        if(Mathf.Abs(transform.position.x - Player.transform.position.x) < Eye_Distance &&
        Mathf.Abs(transform.position.z - Player.transform.position.z) < Eye_Distance){
            Narration_Voice.PlayOneShot(Narration);
            Laser_Light.SetActive(true);
        }
    }
    void OnTriggerEnter(Collider collider){
        if (collider.tag == "Bullet")
        {
            Destroy (collider.gameObject);
            Destroy(this.gameObject);
        }
        // Destroy(this.gameObject);
    }
}

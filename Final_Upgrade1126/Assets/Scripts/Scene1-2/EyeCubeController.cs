﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeCubeController : MonoBehaviour
{
    public GameObject Player;
    public AudioClip Narration;
    public AudioSource Narration_Voice;
    private float Current_Distance;
    public GameObject Laser_Light;
    public float Eye_Distance = 10;
    public float Invoke_time = 5.0f;
    void Start()
    {
        Player = GameObject.Find("PlayerController/first_person_controller/Movingggg");
        Narration_Voice = this.gameObject.GetComponent<AudioSource>();
        Laser_Light = gameObject.transform.GetChild(0).gameObject;
        Laser_Light.SetActive(false);
    }

    void Update()
    {
        NearUpdate();

    }
    void NearUpdate(){
        Current_Distance = Vector3.Distance(Player.transform.position, transform.position);
        if(Current_Distance < 5.0f){
            Narration_Voice.PlayOneShot(Narration);
            Laser_Light.SetActive(true);
            Debug.Log("Near 5");
        }
        if(Mathf.Abs(transform.position.x - Player.transform.position.x) < Eye_Distance &&
        Mathf.Abs(transform.position.z - Player.transform.position.z) < Eye_Distance){
            Narration_Voice.PlayOneShot(Narration);
            Laser_Light.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider collider){
        if (collider.tag == "Bullet"){
            Destroy (collider.gameObject);
            Destroy(this.gameObject);
        }
        Destroy(this.gameObject);
    }
    // void OnTriggerEnter(Collider collider){
    //     if (collider.tag == "Bullet"){
    //         Destroy (collider.gameObject);
    //         Destroy(this.gameObject);
    //     }
    //     Destroy(this.gameObject);
    // }
}

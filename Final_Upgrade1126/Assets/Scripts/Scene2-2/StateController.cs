﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using UnityEngine;

public class StateController : MonoBehaviour
{
    public int Flowers_Count =0;
    public GameObject Flower_1;
    public GameObject Flower_2;
    public GameObject Flower_3;
    // public GameObject Flower_1_killed;
    // public GameObject Flower_2_killed;
    // public GameObject Flower_3_killed;
    public AudioClip Final_Voice_Clip;
    public AudioSource Final_Voice;
    public bool Flower1_Die = false;
    public bool Flower2_Die = false;
    public bool Flower3_Die = false;
    void Start()
    {
        InvokeRepeating("FinalUpdate", 115.0f, 115.0f);
        InvokeRepeating("LevelUpdate", 130.0f, 130.0f);

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(Flowers_Count);
        if(Flowers_Count >= 1 && Flowers_Count <2 && Flower1_Die){
            Debug.Log("1 Flower Die");
            // Flower_1_killed.SetActive(false);
            Flower_2.SetActive(true);
            // Flower_2_killed.SetActive(false);
            // GameObject.Find("flower2").gameObject.SetActive(true);
            // GameObject.Find("flower1").gameObject.SetActive(false);

        }
        else if(Flowers_Count >=2 && Flower2_Die){
            Debug.Log("2 Flower Die");
            // Flower_2_killed.SetActive(false);
            Flower_3.SetActive(true);
        }
    }
    void FinalUpdate(){
        Final_Voice.PlayOneShot(Final_Voice_Clip);
    }
    void LevelUpdate(){
        SceneManager.LoadScene("scene3");

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Flower1;
    public Vector3 New_Flower1_Position;
    public GameObject Flower2;
    public Vector3 New_Flower2_Position;

    public GameObject Flower3;
    public Vector3 New_Flower3_Position;

    public GameObject Tree;
    public GameObject Player;
    
    private Vector3 Previous_Position;
    private Vector3 Current_Position;
    private Vector3 Move_Direction;
    void Start()
    {
        // InvokeRepeating("MoveUpdate", 0.5f, 2.0f);
        Current_Position = Player.transform.position;
        Previous_Position = Player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {



    }
    void MoveUpdate(){
        Current_Position = Player.transform.position;
        Move_Direction = Current_Position - Previous_Position;

        New_Flower1_Position = Flower1.transform.position + Move_Direction;
        Flower1.transform.position = New_Flower1_Position;
        New_Flower2_Position = Flower2.transform.position + Move_Direction;
        Flower2.transform.position = New_Flower2_Position;
        New_Flower3_Position = Flower3.transform.position + Move_Direction;
        Flower3.transform.position = New_Flower3_Position;
        Previous_Position = Current_Position;
    }
}

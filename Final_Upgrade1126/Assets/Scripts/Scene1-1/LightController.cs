﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// /using UnityEngine.Random;

public class LightController : MonoBehaviour
{
    public GameObject Player;
    public const int Lights_UP_Distance = 5;
    const int Lights_Intensity_max = 20000;
    const int Lights_Intensity_min = 10000;
    private int intensity = 0;
    private bool has_trigger = false;

    void Start()
    {
        Player = GameObject.Find("PlayerController (2)/first_person_controller");
        this.gameObject.GetComponent<Light>().intensity = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Mathf.Abs(transform.position.x - Player.transform.position.x) < Lights_UP_Distance &&
        Mathf.Abs(transform.position.z - Player.transform.position.z) < Lights_UP_Distance && 
        has_trigger ==false){
            Debug.Log("Light Trigger");
            intensity = Random.Range(Lights_Intensity_min, Lights_Intensity_max);
            this.gameObject.GetComponent<Light>().intensity = intensity;
            has_trigger = true;
        }
    }

}

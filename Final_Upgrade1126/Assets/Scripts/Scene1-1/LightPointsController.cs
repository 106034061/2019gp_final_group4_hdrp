﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LightPointsController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] lights;
    public GameObject Player;
    public int Light_Transfer_Index = 0;
    private float distance =0;
    public const int Lights_Disappear_Distance = 10;
    const int Lights_Intensity_max = 5000;
    const int Lights_Intensity_min = 0;
    public bool Light_Completed = false;
    public GameObject Scene_Born;
    public GameObject Scene_Grow;
    public GameObject Scene_Mature;
    public GameObject Scene_Remain;
    
    void Start()
    {
        // lights[0] = GameObject.Find("LightPointsController/Point Light");
        // lights[1] = GameObject.Find("LightPointsController/Point Light (1)");
        // lights[2] = GameObject.Find("LightPointsController/Point Light (2)");
        // lights[3] = GameObject.Find("LightPointsController/Point Light (3)");
        // lights[3] = GameObject.Find("LightPointsController/Point Light (4)");
        // lights[4] = GameObject.Find("LightPointsController/Point Light (5)");

    }

    // Update is called once per frame
    void Update()
    {
        if(Light_Transfer_Index < 3){
            if(Mathf.Abs(lights[Light_Transfer_Index].transform.position.x - Player.transform.position.x) < Lights_Disappear_Distance &&
            Mathf.Abs(lights[Light_Transfer_Index].transform.position.z - Player.transform.position.z) < Lights_Disappear_Distance){
                Debug.Log("Light Transfer");
                lights[Light_Transfer_Index].GetComponent<Light>().intensity = Lights_Intensity_min;
                Light_Transfer_Index++;
    	        lights[Light_Transfer_Index].GetComponent<Light>().intensity = Lights_Intensity_max;
                if(Light_Transfer_Index == 1){
                    Scene_Grow.SetActive(true);
                }
                else if(Light_Transfer_Index == 2 ){
                    Scene_Mature.SetActive(true);
                }
                else if(Light_Transfer_Index == 3){
                    Scene_Remain.SetActive(true);
                }


            }
        }
        else{
            lights[3].GetComponent<Light>().intensity = 6000;
            lights[4].GetComponent<Light>().intensity = 6500;
            if(Mathf.Abs(lights[3].transform.position.x - Player.transform.position.x) < Lights_Disappear_Distance &&
            Mathf.Abs(lights[3].transform.position.z - Player.transform.position.z) < Lights_Disappear_Distance){
                Debug.Log("Trust");
                lights[3].GetComponent<Light>().intensity = Lights_Intensity_min;
                SceneManager.LoadScene("scene1-2(1)");
            }
            if(Mathf.Abs(lights[4].transform.position.x - Player.transform.position.x) < Lights_Disappear_Distance &&
            Mathf.Abs(lights[4].transform.position.z - Player.transform.position.z) < Lights_Disappear_Distance){
                Debug.Log("Not trust");
                lights[4].GetComponent<Light>().intensity = Lights_Intensity_min;
                SceneManager.LoadScene("scene1-2(2)");

            }
        }

    }
}

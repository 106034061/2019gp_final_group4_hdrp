﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FPC : MonoBehaviour {
  [SerializeField] private float movementSpeed;
  [SerializeField] private float slopeForce;
  [SerializeField] private float slopeForceRayLength;
  [SerializeField] private AnimationCurve jumpFallOff;
  [SerializeField] private float jumpMultiplier;
  [SerializeField] private KeyCode jumpKey;
  private bool isJumping;
  private CharacterController charController; private void Awake() { charController = GetComponent<CharacterController>(); }

  public GameObject stone;
  public GameObject stonePointLight;
  public GameObject tower2;
  public GameObject pointLight2;
  public GameObject mainLight2;
  public GameObject finalLight;
  public GameObject playerController;
  public GameObject rope;
  public float stoneMoveTime;
  public float finalLightTime;
  public float fallingAcceleration;
  public Vector3 stoneoffset;

  private float stoneTimer;
  private int stoneState = 0;

  private void Update() {
    PlayerMovement();
    if (transform.position.y < 3) SceneManager.LoadScene("scene2-2");
    if (stoneState == -1) {
      if (stoneTimer < 2f) {
        stoneTimer += Time.deltaTime;
      } else {
        stoneState = 1;
        stoneTimer = 0f;
        tower2.SetActive(true);
        pointLight2.SetActive(true);
        playerController = transform.parent.gameObject;
        transform.SetParent(stone.transform);
        stone.GetComponent<Animator>().SetBool("Moving", true);
      }
    } else if (stoneState == 1) {
      stoneTimer += Time.deltaTime;
      if (stoneTimer > stoneMoveTime + 0.8f) {
        stoneState = 2;
        transform.SetParent(playerController.transform);
      } else {
        if ((stone.transform.position - transform.position).magnitude > 0.35) {
          transform.position = stoneoffset + stone.transform.position;
        }
      }
    } else if (stoneState == 3) {
      for (int i = 0; i < 4; ++i) pointLight2.transform.GetChild(i).gameObject.GetComponent<Light>().intensity = Mathf.Lerp(0, 15.9f, stoneTimer / finalLightTime);
      mainLight2.transform.GetChild(0).gameObject.GetComponent<Light>().intensity = Mathf.Lerp(0, 250f / 100f * 8f, stoneTimer / finalLightTime);
      mainLight2.transform.GetChild(1).gameObject.GetComponent<Light>().intensity = Mathf.Lerp(0, 200f / 100f * 8f, stoneTimer / finalLightTime);
      mainLight2.transform.GetChild(2).gameObject.GetComponent<Light>().intensity = Mathf.Lerp(0,  30f / 100f * 8f, stoneTimer / finalLightTime);
      finalLight.transform.GetChild(0).gameObject.GetComponent<Light>().intensity = Mathf.Lerp(0, 600f / 100f * 8f, stoneTimer / finalLightTime);
      finalLight.transform.GetChild(1).gameObject.GetComponent<Light>().intensity = Mathf.Lerp(0, 200f / 100f * 8f, stoneTimer / finalLightTime);
      stoneTimer += Time.deltaTime;
      if (stoneTimer > finalLightTime + 0.8f) {
        rope.SetActive(true);
        stoneState = 4;
      }
    } else if (stoneState == 4) {
      if (Input.GetKey(KeyCode.Space)) {
        stoneState = 5;
        stoneTimer = 0;
        playerController.transform.GetChild(1).gameObject.GetComponent<Animator>().enabled = true;
        playerController.transform.GetChild(1).gameObject.GetComponent<Animator>().SetBool("Moving", true);
        transform.localRotation = Quaternion.identity;
      }
    } else if (stoneState == 5) {
      if (transform.position.z > 6.2f) SceneManager.LoadScene("scene2-2");
      for (int i = 0; i < 10; ++i) {
        if (stoneTimer < i && stoneTimer + Time.deltaTime >= i) {
          playerController.transform.GetChild(1).gameObject.GetComponent<Animator>().speed *= fallingAcceleration;
        }
      }
      stoneTimer += Time.deltaTime;
    }
  }

  private void PlayerMovement() {
    float horizInput = Input.GetAxisRaw("Horizontal");
    float vertInput = Input.GetAxisRaw("Vertical");
    Vector3 forwardMovement = transform.forward * vertInput;
    Vector3 rightMovement = transform.right * horizInput;
    charController.SimpleMove(Vector3.ClampMagnitude(forwardMovement + rightMovement, 1.0f) * movementSpeed);
    if ((vertInput != 0 || horizInput != 0) && OnSlope()) {
      charController.Move(Vector3.down * charController.height / 2 * slopeForce * Time.deltaTime);
    }
    JumpInput();
  }

  private bool OnSlope() {
    if (isJumping) return false;
    RaycastHit hit;
    if (Physics.Raycast(transform.position, Vector3.down, out hit, charController.height / 2 * slopeForceRayLength))
      if (hit.normal != Vector3.up)
        return true;
    return false;
  }

  private void JumpInput() {
    if(Input.GetKeyDown(jumpKey) && !isJumping) { isJumping = true;
      StartCoroutine(JumpEvent());
    }
  }

  private IEnumerator JumpEvent() {
    charController.slopeLimit = 90.0f;
    float timeInAir = 0.0f;
    do {
      float jumpForce = jumpFallOff.Evaluate(timeInAir);
      charController.Move(Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime);
      timeInAir += Time.deltaTime;
      yield return null;
    } while (!charController.isGrounded && charController.collisionFlags != CollisionFlags.Above);
    charController.slopeLimit = 45.0f;
    isJumping = false;
  }

  public void OnControllerColliderHit(ControllerColliderHit collision) {
    if (stoneState == 0 && collision.gameObject.tag == "Stone") {
      stoneState = -1;
      stoneTimer = 0f;
      stonePointLight.SetActive(true);
      stoneoffset = transform.position - stone.transform.position;
    } else if (stoneState == 2 && collision.gameObject.tag == "Tower2") {
      stoneState = 3;
      stoneTimer = 0f;
      mainLight2.SetActive(true);
      for (int i = 0; i < 3; ++i) mainLight2.transform.GetChild(i).gameObject.GetComponent<Light>().intensity = 0f;
      finalLight.SetActive(true);
      for (int i = 0; i < 2; ++i) finalLight.transform.GetChild(i).gameObject.GetComponent<Light>().intensity = 0f;
      stonePointLight.SetActive(false);
    }
  }
}
